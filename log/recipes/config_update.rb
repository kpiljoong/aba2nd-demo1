
execute "Update CloudWatch Logs agent config file" do
  command "/opt/aws/cloudwatch/awslogs-agent-setup.py -r us-east-1 -c /tmp/cwlogs.cfg"
  not_if { system "pgrep -f aws-logs-agent-setup" }
end

service "awslogs" do
  action :restart
end

